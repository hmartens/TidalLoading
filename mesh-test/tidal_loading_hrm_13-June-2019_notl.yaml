# Generate a Tidal Loading Mesh.
mesh_type: TidalLoading

# The most important parameters.
basic:
    # Model Name for predefined models or filename for external model
    # file.
    model: "prem_iso_one_crust"

    # Number of elements along one edge of the cubed sphere at the
    # surface
    nex: 36

    # index of the discontinuity below which to refine.
    refinement_index: 6

    # Filename for topography, generated with salvus_topo:
    # 'https://gitlab.com/Salvus/salvus_topo'
    #tidal_loading_file: "./tidal_elevation_filtered_TPX09_m2.nc"
    tidal_loading_file: "./tidal_elevation_filtered.nc"

    # tidal loading max l in region of interest
    tidal_loading_lmax_1: 256

    # tidal loading max l outside of region of interest
    tidal_loading_lmax_2: 128

    # Region of interest, [lat, lon, radius, taper width] in degree
    region_of_interest: [-11.0, -46.0, 10.0, 5.0]

    # Number of elements in the topmost layers
    nelem_vertical: []

    # Level of local gradient based edge refinement at the surface.
    local_refinement_level: 1

    # Level of global gradient based edge refinement at the surface.
    global_refinement_level: 0

    # Threshold for local refinement.
    refinement_threshold: 0.01


# Advanced parameters for more control.
advanced:
    # Model parameters to put into the mesh, defaults to the
    # parametrization used by the background model.
    model_parameters: []

    # How to represent the velocity model on the mesh. element_nodes:
    # values are stored on each node of each element, allows
    # discontinuities and (bi/tri)linear variations, but produces
    # large files. elements: one value per element, allows
    # discontinuities but the model will be piecewise constant. nodes:
    # one value per node, enforces a continuous model, using mean at
    # values on discontinuities. Choices: [element_nodes, elements,
    # nodes]
    velocity_model_representation: "element_nodes"

    # Spline order used for model interpolation in case of layered
    # models. Ignored for PREM like polynomial models.
    model_spline_order: 3

    # If true, the final mesh will be compressed with gzip compression
    # level 2. Compression is for the most part only interesting for
    # constant values and the compression level does matter much in
    # that case.
    compression: True


# Adding topography to the mesh.
topography:
    # Filename for topography, generated with salvus_topo:
    # 'https://gitlab.com/Salvus/salvus_topo'
    topography_file: ""

    # lmax of the topography to be used.
    topography_lmax: 128

    # Filename for moho topography, generated with salvus_topo:
    # 'https://gitlab.com/Salvus/salvus_topo'
    moho_topography_file: ""

    # lmax of the moho topography to be used.
    moho_topography_lmax: 128


# Parameters for spherical meshes.
spherical:
    # Minimum radius in km.
    min_radius: 0.0

    # Ellipticity of the planet (WGS84: 0.0033528106647474805, GRS80:
    # 0.003352810681182319, MARS: 0.00589)
    ellipticity: 0.0


# Adding the Exterior Domain for Gravity Computation. Implies including the whole planet with core.
gravity_mesh:
    # Switch for addition of exterior domain.
    add_exterior_domain: False

    # Coarsen at CMB.
    coarsen_cmb: True

    # Number of coarsening layers between surface and outer boundary.
    ncoarse: 1

    # Number of buffer elements between the surface and the first
    # coarsening layer.
    nelem_buffer_surface: 1

    # Number of buffer elements between the last coarsening layer and
    # the outer boundary.
    nelem_buffer_outer: 5

    # Basis for power law dr in outer layer.
    dr_basis: 1.5


