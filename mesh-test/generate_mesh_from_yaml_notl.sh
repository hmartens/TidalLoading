# Generate mesh
python -m salvus_mesh.interface --input_file tidal_loading_hrm_13-June-2019_notl.yaml --overwrite

# Rename file
fname_original='TidalLoading_prem_iso_one_crust_36.e'
fname_new='TidalLoading_prem_iso_one_crust_36_local1_gravFalse_13_06_19.e'
mv ${fname_original} ${fname_new}

# Copy file to Jokull
#scp ${fname_new} hmartens@jokull.gps.caltech.edu:/home/bak/hmartens/ETH/2019/tidal_loading/meshes/
