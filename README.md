# TidalLoading

While still lacking any form of documentation, this is at least a starting point to collect useful information

## List of options for the simulation 


``` bash
###########################################################################################
# Change the number of nodes above according to the number of elements
# As a rule of thumb there shouldn't be more than 100 k elements per node
#
#
### Mandatory options #####################################################################
# --mesh-file                    Exodus file containing the mesh and the model
# --model-file                   Exodus file containing the mesh and the model
# --polynomial-order             Polynomial degree of the FE shape functions (1, 2 or 4)
# --dimension                    Dimension of the mesh (2 or 3)
# --static-problem               Flag to switch on static solver
#
### Problem-specific options ##############################################################
#
## body tides
# --orbiter-mass				 Mass of the moon (scalar)
# --orbiter-location			 Location of the center of the moon in m 
#                                relative to the center of the Earth (comma separated vector)
#
## tidal loading 
# --neumann-boundaries			 Side set name where the Neumann boundaries should be applied (r1)
#
## to specify the load either use
# --spherical-surface-load-radius     Radius [m] of the spherical cap 
# --spherical-surface-load-amplitude  Amplitude of the spherical cap 
## or
# --surface-load-field                Field name in the mesh file containing the tidal force
#
## regularization
# --homogeneous-dirichlet        Side set name where homogeneous Dirichlet conditions should be applied (r0)
#                                This only works if the mesh has a whole in the middle 
#                                (e.g., either the core is missing or the minimum radius is greater than zero)
#                                The Dirichlet condition will fix the location of the body in the interior,
#                                which makes the problem well-posed.
#
### Optional output #######################################################################
#
## volumetric output
# --save-fields                  Fields for volumetric output (currently the only option is u_ELASTIC)
# --save-wavefield-file          Output file name (optional, default: wavefield.h5)
## surface output
# --save-boundaries              Side set names of the boundaries to store
# --save-boundary-fields         Fields to store at the boundary (surface)
# --save-boundaries-file         Output file name (optional, default: wavefield.h5)
#
### PETSC options for linear solver
# -ksp_type                      Select linear solver type (cg, gmres, ...)
# -ksp_max_it                    Maximum number of iterations
# -ksp_rtol                      Relative tolerance
# -ksp_monitor                   Flag to get iterations output
# -ksp_converged_reason          Print termination criteria
# -log_view                      Verbose output
```


## Examples

### Body tides

Solid Earth tide caused by the Moon

```
--mesh-file mesh.e \
--model-file mesh.e \
--dimension 3 \
--polynomial-order 1 \
--static-problem \
--homogeneous-dirichlet r0 \
--orbiter-mass 7.347673e22 \
--orbiter-location 0.0,384400.0e3,0.0 \
--verbose \
--save-fields u_ELASTIC \
-ksp_type cg \
-ksp_max_it 8000 \
-ksp_rtol 1e-10 \
-ksp_monitor \
-ksp_converged_reason \
-log_view
```

### Tidal loading

Applying two spherical caps as surface load on North and South pole

```
--mesh-file mesh.e \
--model-file mesh.e \
--dimension 3 \
--polynomial-order 1 \
--static-problem \
--homogeneous-dirichlet r0 \
--neumann-boundaries r1 \
--spherical-surface-load-radius 1000000.0 \
--spherical-surface-load-amplitude 1.0 \
--save-boundaries r1 \
--save-boundary-fields u_ELASTIC \
--verbose \
-ksp_type cg \
-ksp_max_it 8000 \
-ksp_rtol 1e-10 \
-ksp_monitor \
-ksp_converged_reason \
-log_view
```

Applying two spherical caps as surface load on North and South pole

```
--mesh-file mesh.e \
--model-file mesh.e \
--dimension 3 \
--polynomial-order 1 \
--static-problem \
--homogeneous-dirichlet r0 \
--neumann-boundaries r1 \
--surface-load-field surface_load \
--verbose \
-ksp_type cg \
-ksp_max_it 8000 \
-ksp_rtol 1e-10 \
-ksp_monitor \
-ksp_converged_reason \
-log_view
```